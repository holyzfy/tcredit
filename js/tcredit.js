(function(){

    document.onkeydown=function(event){
		var e = event || window.event || arguments.callee.caller.arguments[0];
		if(e && e.keyCode==13){ // enter 键
		//要做的事情
			$("#btnQuery").trigger("click");
		}
	}; 

    $(".close").live("click",function(){
        $('.theme-mask').hide();
        $('.popover1').slideUp(200);
        
    }); 
    $(".queren").live("click",function(){
        $('.theme-mask').hide();
        $('.popover1').slideUp(200);        
    }); 

    $("#btnQuery").live("click",function(){
    	var query = $.trim($("#txtQuery").val());
    	if(query!=''){
			window.location.href="http://q.tcredit.com/searchresult?q="+query;
    	}else{
    		alert('请输入需要查询的公司名称！');
    	}
    });

    $("#btnPostContact").live("click",function(){
    	var company = $.trim($("#company").val());
    	var name = $.trim($("#name").val());
    	var email = $.trim($("#email").val());
    	var mobile = $.trim($("#mobile").val());
    	if(company==''||name==''||email==''||mobile==''){
    		alert('请完善联系信息！');
    	}else{
            if(!validateEmail(email)){
                alert('请填写正确的邮箱！');
                return false;
            }
            if(!validateMobile(mobile)){
                alert('请填写正确的手机号码！');
                return false;
            }
    		var url = "http://admin.tcredit.com/bigdata/manage/addBusinessCooperation";
    		var data = {
    			customerName : name,
    			email : email,
    			mobile : mobile,
    			companyName : company
    		}
    		// $.ajax({
    		// 	async:false,
   			// 	url: url,
   			// 	type: "GET",
   			// 	dataType: 'jsonp',
   			// 	jsonp: 'jsoncallback',
   			// 	data: qsData,
			   //  success: function (json) {
			   //  	if(response.status==0){
	    	// 			alert(response.message);
	    	// 		}else{
	    	// 			alert(response.message);
	    	// 		}
			   //  }
    		// });
    		$.post(url,data,function(response){
    			if(response.status==0){
                    $('.theme-mask').show();
                    $('.theme-mask').height($(document).height());
                    $('.popover1').slideDown(200);
    			}else{
    				alert('对不起，您已经保存过该联系方式!');
    			}
    		},'json');
    	}
    });
})();

function validateEmail(email){
    var reg = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    return reg.test(email);
}

function validateMobile(mobile){
    var reg = /^0?1[3|4|5|8][0-9]\d{8}$/;
    return reg.test(mobile);
}

